;HEADER
;Start_gcode
G21                     ; Set to metric [change to G20 if you want Imperial]
G90                     ; Force coordinates to be absolute relative to the origin
G28                     ; Home X/Y/Z axis
M117 Printing...
;ENDHEADER
;Layer #
G0 X249.62 Y215.15 Z90.00 F2500
G1 X249.62 Y215.15 Z15.00 F3000
G1 X360.87 Y493.27 Z15.00 F3000
G1 X463.93 Y212.69 Z15.00 F3000
G1 X213.63 Y409.83 Z15.00 F3000
G1 X502.38 Y408.19 Z15.00 F3000
G1 X249.62 Y215.15 Z15.00 F3000
G0 X249.62 Y215.15 Z90.00 F2500
;FOOTER
; end_gcode
G28 X Y
G0 Z100 				; HOME
G90                             ; absolute positioning
;ENDFOOTER
