void setup() {
  size(500, 400);
  background(10, 80, 100);
}

void draw() {
  if (mousePressed) {
    background(10, 80, 100);
  } 

  stroke(255, 255, 255);
  fill(mouseX, mouseY, mouseY);
  ellipse(mouseX, mouseY, mouseX-30, mouseY);
}
